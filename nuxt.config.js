export default {
  /**
   * access via: "process.env.webModule"
   * https://nuxtjs.org/docs/2.x/directory-structure/nuxt-config
   */
  env: {
    webModule: require("./package.json").name,
    development: process.env.NODE_ENV !== 'production'
  },

  /**
   * global config
   * router:  https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-router
   * srcDir:  https://nuxtjs.org/docs/2.x/directory-structure/nuxt-config#srcdir
   * alias:   https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-alias
   * ssr:     https://go.nuxtjs.dev/ssr-mode
   * target:  https://go.nuxtjs.dev/config-target
   */
  server: {
    port: 3000, // default: 3000
  },
  router: {
    base: "/",
    middleware: ["PageTracking"],
  },
  srcDir: "src/",
  alias: {
    "~~/*": "./*",
    "~/*": "./src/*", // see also tsconfig.json
  },
  ssr: false,
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Player Media Monitor',
    htmlAttrs: {
      lang: 'de'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: "timestamp", content: Date.call()},
      {name: "environment", content: process.env.NODE_ENV},
      {name: "auth-domain", content: process.env.AUTH0_DOMAIN},
      {
        name: "version",
        content: require("./package.json").version,
      },
    ],
    link: [{rel: "icon", type: "image/x-icon", href: "/favicon.ico"}],
  },

  /**
   * build config
   * css:     https://go.nuxtjs.dev/config-css
   * loading: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-loading#using-a-custom-loading-component
   * plugins: https://go.nuxtjs.dev/config-plugins
   * import components: https://go.nuxtjs.dev/config-components
   * buildModules: https://go.nuxtjs.dev/config-modules (recommended)
   */
  css: ['~/assets/scss/general.scss'],
  styleResources: {
    scss: ['~/assets/scss/resources.scss']
  },
  plugins: [
    '~/plugins/Axios-accessor.ts'
  ],
  components: true,
  buildModules: [
    '@nuxt/typescript-build',
    ['@nuxtjs/moment', {
      defaultLocale: 'de',
      locales: ['de'],
      timezone: true
    }]
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/auth-next',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    'nuxt-i18n'
  ],

  axios: {
    proxy: true,
  },

  /*
 * module "@nuxtjs/proxy"
 * https://www.npmjs.com/package/@nuxtjs/proxy
 * https://axios.nuxtjs.org/options/
 */
  proxy: {
    "/api": {
      target: "http://127.0.0.1:3001", // local
    },

    // "/api/kpi": {
    //   target: "http://127.0.0.1:8080/web", // redirect to KPI service
    //   pathRewrite: { "^/api/kpi/": "" },
    // },
  },

  // https://auth.nuxtjs.org/providers/auth0
  auth: {
    redirect: {
      login: '/', // redirect user when not connected
      callback: '/signin' // User will be redirected to this path by the identity provider after login
    },
    strategies: {
      auth0: {
        domain: process.env.AUTH0_DOMAIN,
        clientId: process.env.AUTH0_CLIENT_ID,
        audience: process.env.AUTH0_AUDIENCE,
        logoutRedirectUri: 'http://localhost:3000/', // TODO: dev ≠ production
        scope: ['openid', 'profile', 'email'],
        responseType: 'code',
        grantType: 'authorization_code',
        codeChallengeMethod: 'S256',
      }
    },
    plugins: [
      {src: '~/plugins/Axios.ts', mode: 'client'},
      {src: '~/plugins/Permission.ts', mode: 'client'},
      {src: '~/plugins/Role.ts', mode: 'client'},
      {src: "~/plugins/Rest.ts", mode: "client"}
    ]
  },


  // module i18n
  // https://github.com/nuxt-community/i18n-module
  i18n: {
    locales: [
      {code: 'en', name: 'English', iso: 'en-US', file: 'en.json'},
      {code: 'de', name: 'Deutsch', iso: 'de-DE', file: 'de.json'}
    ],
    langDir: '~/locales/',
    defaultLocale: 'de',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      onlyOnRoot: true,  // recommended
    },
    strategy: 'no_prefix'
  },
}
