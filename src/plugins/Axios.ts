/**
 * set user language in http header
 * @param $axios
 * @param i18n
 */
export default ({$axios, i18n}: any) => {
    $axios.setHeader("Language", i18n.getLocaleCookie());
};
