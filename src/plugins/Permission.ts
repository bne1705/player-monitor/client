import JWT from "~/utils/JWT";
import {Inject} from "@nuxt/types/app";

export default ({$axios, $auth}: any, inject: Inject) => {

  const factory: any = {

    /**
     * check for admin permissions 'admin:read'
     */
    isAdmin: () => {
      let isAdmin: boolean = false;
      let token: string = $auth.strategy.token.get();

      if(token){
        let user: any = JWT.parse(token);
        if (user && user['permissions']) {
          if (user['permissions'].indexOf('admin:read') !== -1) {
            isAdmin = true;
          }
        }
      }

      return isAdmin;
    }
  }

  inject('permission', factory);
}
