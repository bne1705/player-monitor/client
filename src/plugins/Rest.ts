import {Inject} from "@nuxt/types/app";
import {Context} from "@nuxt/types";
import Stream from "~/utils/rest/Stream";
import Tracking from "~/utils/rest/Tracking";
import Me from "~/utils/rest/Me";

/**
 * global access to api with dependencies (axios, auth, store)
 * inspired here:
 * - https://blog.lichter.io/posts/nuxt-api-call-organization-and-decoupling/
 * - https://www.telerik.com/blogs/api-factories-vue-nuxt
 *
 * @param ctx - the whole nuxt context
 * @param inject
 */
export default (ctx: Context, inject: Inject) => {
  const factory: any = {
    Stream: Stream(ctx),
    Tracking: Tracking(ctx),
    Me: Me(ctx)
  };

  inject("rest", factory);
};
