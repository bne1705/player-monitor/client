import JWT from "~/utils/JWT";
import {Inject} from "@nuxt/types/app";
import {Roles} from "~/utils/enums/Roles";

export default ({$axios, $auth}: any, inject: Inject) => {

  const roleProp: string = 'http://tstv.roles.module/roles';

  const factory: any = {

    /**
     * check for user role
     */
    available: (requestedRole: Roles) => {
      let access: boolean = false;
      let token: string = $auth.strategy.token.get();

      if(token){
        let user: any = JWT.parse(token);
        if (user && user[roleProp]) {
          if (user[roleProp].indexOf(requestedRole) !== -1) {
            access = true;
          }

          if (user[roleProp].indexOf(Roles.ADMIN) !== -1) {
            access = true;
          }
        }
      }

      return access;
    }
  }

  inject('role', factory);
}
