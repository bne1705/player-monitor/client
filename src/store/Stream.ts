import {find} from "lodash";
import {Subscription} from "~/utils/interfaces/Subscription";
import Article from "~/utils/interfaces/Article";
import StreamResponse from "~/utils/interfaces/StreamResponse";
import Stream from "~/utils/rest/Stream";

export const state = () => ({
  searchValue: '',
  stream: {
    articles: [],
    tops: []
  },
  subscriptions: []
});


export const getters = {
  subscriptions: (state: any) => state.subscriptions,
  stream: (state: any) => state.stream.articles,
  tops: (state: any) => state.stream.tops,

  id: (state: any) => (id: string) => {
    return find(state.subscriptions, (o: Subscription) => {
      return o.id === id;
    });
  }
}


export const mutations = {
  STREAM: (state: any, stream: Article[]) => state.stream.articles = stream,
  TOPS: (state: any, tops: Article[]) => state.stream.tops = tops,

  UPDATE_SEARCHVALUE: (state: any, searchValue: string) => state.searchValue = searchValue,

  RESET_SEARCHVALUE: (state: any) => state.searchValue = '',
  RESET_STREAM: (state: any) => state.stream = {articles: [], tops: []},

  UPDATE_SUBSCRIPTIONS(state: any, subscriptions: Subscription[]) {
    state.subscriptions = subscriptions;
  }
}


export const actions = {
  /**
   * fetch stream
   * @param commit
   * @param state
   * @param payload
   * @param $axios
   */
  async fetchStream({commit, state}: any, payload: any) {
    commit('UPDATE_SEARCHVALUE', payload.searchValue);

    let stream: Article[] = [];
    let streamResp: StreamResponse | null = await Stream({$axios: payload.axios}).news(state.searchValue);
    if (streamResp) {
      stream = streamResp.articles;
    }
    commit('STREAM', stream);

    let tops: Article[] = [];
    let topsResp: StreamResponse | null = await Stream({$axios: payload.axios}).top(state.searchValue);
    if (topsResp) {
      tops = topsResp.articles;
    }
    commit('TOPS', tops);
  },

  /**
   * reset store
   * @param commit
   */
  reset({commit}: any) {
    commit('RESET_STREAM');
    commit('RESET_SEARCHVALUE');
  }
}
