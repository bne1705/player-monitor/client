export default class Logger {

    /**
     * show highlighted log in console
     * eg Logger.info('name', 'a message', {some: 'data'}, '#f00');
     *
     * @param name
     * @param message
     * @param data
     * @param color
     */
    public static info(
        name: string,
        message: string,
        data: any,
        color: string = '#16bbe9'
    ) {
        console.info(`%c [${name}] ${message}`, `background: ${color}; color: #f3f3f3`, data);
    }
}
