export default class WebStorage {

    public static devider: string = '.';
    public static prefix: string = require("~~/package.json").name;

    /**
     * usage: Storage.cookie.get|set|delete|clear()
     */
    public static cookie = {

        /**
         * should only use internaly (private)
         * @param c
         */
        exec: (c: cookie) => {
            document.cookie = `${c.name}=${c.value}; expires=${c.expires}; path=${c.path};`;
        },

        /**
         * get a cookie
         * @param name
         */
        get: (name: any): any => {
            return WebStorage.cookie.getRawCookie(WebStorage.prefix + WebStorage.devider + name);
        },

        /**
         * set a cookie
         * @param name
         * @param data
         * @param expireDateString
         */
        set: (name: string, data: any, expireDateString?: any) => {
            let cookie: cookie = {
                name: WebStorage.prefix + WebStorage.devider + name,
                value: (typeof data === 'string') ? data : JSON.stringify(data),
                path: '/',
                expires: new Date(expireDateString)
            };

            WebStorage.cookie.exec(cookie);
        },


        /**
         * remove a cookie
         * @param name
         */
        delete: (name: string) => {
            let cookie: cookie = {
                name: WebStorage.prefix + WebStorage.devider + name,
                value: "",
                path: '/',
                expires: new Date('Thu, 01 Jan 1970 00:00:00 UTC')
            };

            WebStorage.cookie.exec(cookie);
        },


        /**
         * clear all cookies
         */
        clear: () => {
            let allCookiesTerm = document.cookie;
            for (const cookieString of allCookiesTerm.split(';')) {
                let cookieTrim = cookieString.trim();
                let cookieObj = cookieTrim.split('=');

                let cookie: cookie = {
                    name: cookieObj[0],
                    value: "",
                    path: '/',
                    expires: new Date('Thu, 01 Jan 1970 00:00:00 UTC')
                };

                WebStorage.cookie.exec(cookie);
            }
        },


        /**
         * get a cookie without namespace / prefix
         * @param name
         */
        getRawCookie: (name: any) => {
            const value: any = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');
            return value ? decodeURIComponent(value.pop()) : '';
        },


        /**
         * set cookie without namespace / prefix
         * @param name
         * @param data
         * @param expireDateString
         */
        setRawCookie: (name: string, data: any, expireDateString?: any) => {
            let cookie: cookie = {
                name: name,
                value: (typeof data === 'string') ? data : JSON.stringify(data),
                path: '/',
                expires: new Date(expireDateString)
            };

            WebStorage.cookie.exec(cookie);
        },
    }


    /**
     * usage: Storage.local.get|set|delete|clear()
     */
    public static local = {
        /**
         *
         * @param name
         * @param internal
         */
        get: (name: string, internal: boolean = true) => {
            let nameProp: string = (internal) ? WebStorage.prefix + WebStorage.devider + name : name;
            let item: any = localStorage.getItem(nameProp);

            try {
                return JSON.parse(item);
            } catch (e) {
                return item;
            }
        },


        /**
         *
         * @param name
         * @param data
         * @param internal
         */
        set: (name: string, data: any, internal: boolean = true) => {
            let values: any|null = null;
            let nameProp: string = (internal) ? WebStorage.prefix + WebStorage.devider + name : name;

            if (typeof data === 'string') {
                values = data;
            } else {
                values = JSON.stringify(data);
            }

            localStorage.setItem(nameProp, values);
        },


        /**
         *
         * @param name
         * @param internal
         */
        delete: (name: string, internal: boolean = true): boolean => {
            let nameProp: string = (internal) ? WebStorage.prefix + WebStorage.devider + name : name;
            localStorage.removeItem(nameProp);

            return true;
        },


        /**
         *
         */
        clear: (): boolean => {
            localStorage.clear();
            return true;
        }
    }


    /**
     * usage: Storage.session.get|set|delete|clear()
     */
    public static session = {
        /**
         *
         * @param name
         */
        get(name: string) {
            let item: any = sessionStorage.getItem(WebStorage.prefix + WebStorage.devider + name);

            try {
                return JSON.parse(item);
            } catch (e) {
                return item;
            }
        },


        /**
         *
         * @param name
         * @param data
         */
        set(name: string, data: any) {
            let values: any = null;

            if (typeof data === 'string') {
                values = data;
            } else {
                values = JSON.stringify(data);
            }

            sessionStorage.setItem(WebStorage.prefix + WebStorage.devider + name, values);
        },


        /**
         *
         * @param name
         */
        delete(name: string) {
            sessionStorage.removeItem(WebStorage.prefix + WebStorage.devider + name);
            return true;
        },


        /**
         *
         */
        clear() {
            sessionStorage.clear();
            return true;
        }
    }

}


interface cookie {
    name: string,
    value: string,
    path: string,
    expires: Date,
}
