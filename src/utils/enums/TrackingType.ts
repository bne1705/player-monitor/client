export enum TrackingType {
    PAGE = "page",
    CLICK = "click",
    EVENT = "event"
}