export default interface RestResponse {
    success: boolean
    result: any
}
