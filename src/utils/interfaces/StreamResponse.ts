import Article from "~/utils/interfaces/Article";

export default interface StreamResponse {
  articles: Article[]
  status: string
  totalResults: number
}
