export interface Subscription {
  id: string
  name: string
  updatedAt: string
  searchValue: string
  user?: number | null
}
