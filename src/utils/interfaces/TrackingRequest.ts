import {TrackingType} from "~/utils/enums/TrackingType";

export interface TrackingRequest {
    type: TrackingType
    path: string
    moduleName: string
    target?: string
    context?: string
    resolution?: number
    userAgent?: string
}