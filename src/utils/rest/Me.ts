import {Subscription} from "~/utils/interfaces/Subscription";
import RestResponse from "~/utils/interfaces/RestResponse";

export default ({$axios}: any) => ({

  config: {

    set: async (doc: { id: string | null, name: string }): Promise<Subscription[]> => {
      let response: RestResponse = await $axios.$post(`/api/me/subscription`, doc);

      if (response.success) {
        return response.result['doc']['config'];
      } else {
        return [];
      }
    },

    get: async (): Promise<Subscription[]> => {
      let response: RestResponse = await $axios.$get(`/api/me/subscription`);

      if (response.result) {
        return response.result['config'];
      } else {
        return [];
      }
    },

    remove: async (id: string): Promise<Subscription[]> => {
      let response: RestResponse = await $axios.$delete(`/api/me/subscription`, {data: {id}});

      if (response.success) {
        return response.result['doc']['config'];
      } else {
        return [];
      }
    }

  }

});
