import RestResponse from "~/utils/interfaces/RestResponse";
import StreamResponse from "~/utils/interfaces/StreamResponse";

export default ({$axios}: any) => ({

  /**
   * fetch all news
   * @param searchValue
   */
  news: async (searchValue: string): Promise<StreamResponse | null> => {
    let response: RestResponse = await $axios.$post("/api/stream", {
      searchValue
    });

    if (response.success) {
      return response.result;
    } else {
      return null;
    }
  },

  /**
   * fetch top news
   * @param searchValue
   */
  top: async (searchValue: string): Promise<StreamResponse | null> => {
    let response: RestResponse = await $axios.$post("/api/stream/top", {
      searchValue
    });

    if (response.success) {
      return response.result;
    } else {
      return null;
    }
  }

});
