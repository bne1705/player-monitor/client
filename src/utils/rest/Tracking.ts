import {TrackingType} from "~/utils/enums/TrackingType";
import RestResponse from "~/utils/interfaces/RestResponse";
import {TrackingRequest} from "~/utils/interfaces/TrackingRequest";
import Logger from "~/utils/Logger";

export default ({$axios, env}: any) => ({

    /**
     * Page Tracking
     */
    page: async (): Promise<RestResponse> => {
        const data: TrackingRequest = {
            type: TrackingType.PAGE,
            path: location.href,
            moduleName: env.webModule,
            resolution: window.innerWidth,
            userAgent: navigator.userAgent
        }

        Logger.info('Tracking', `"${TrackingType.PAGE}"`, data);
        return await $axios.$post("/api/tracking", data);
    },

    /**
     * Click Tracking
     * @param target
     * @param context
     */
    click: async (target: string, context: string): Promise<RestResponse> => {
        const data: TrackingRequest = {
            type: TrackingType.CLICK,
            path: location.href,
            moduleName: env.webModule,
            target,
            context
        }

        Logger.info('Tracking', `"${TrackingType.CLICK}"`, data);
        return await $axios.$post("/api/tracking", data);
    },

});
